package com.example.demo.dto;

public enum Category {
	
	History,
	Fictions,
	Horror,
	Philosophy

}
