package com.example.demo.dto;

public enum Status {
	AVAILABLE, BORROWED, OVERDUE

}
